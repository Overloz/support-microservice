FROM maven:3.8.1-jdk-11-slim AS build
WORKDIR /usr/app/
COPY pom.xml /usr/app/
RUN mvn verify clean --fail-never
COPY src /usr/app/src/
RUN mvn package

FROM maven:3.8.1-jdk-11-slim
COPY --from=build /usr/app/target/support.jar support.jar
ENTRYPOINT ["java","-jar","/support.jar"]