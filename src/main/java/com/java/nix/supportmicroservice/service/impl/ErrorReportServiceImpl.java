package com.java.nix.supportmicroservice.service.impl;

import com.java.nix.supportmicroservice.entity.ErrorReport;
import com.java.nix.supportmicroservice.entity.dto.CourseDto;
import com.java.nix.supportmicroservice.entity.dto.ErrorReportDto;
import com.java.nix.supportmicroservice.repository.ErrorReportRepository;
import com.java.nix.supportmicroservice.service.CourseService;
import com.java.nix.supportmicroservice.service.ErrorReportService;
import com.java.nix.supportmicroservice.service.SplitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ErrorReportServiceImpl implements ErrorReportService {

    private final CourseService courseService;

    private final SplitService splitService;

    private final ErrorReportRepository errorReportRepository;

    @Autowired
    public ErrorReportServiceImpl(CourseService courseService, SplitService splitService,
            ErrorReportRepository errorReportRepository) {
        this.courseService = courseService;
        this.splitService = splitService;
        this.errorReportRepository = errorReportRepository;
    }

    @Override
    public ErrorReport getById(Long id) {
        return errorReportRepository.findById(id).orElse(null);
    }

    @Override
    public List<ErrorReport> getAll() {
        return errorReportRepository.findAll();
    }

    @Override
    public List<ErrorReport> getAllByCourseId(String courseId) {
        return errorReportRepository.findAllByCourseId(courseId);
    }

    @Override
    public ErrorReport saveReport(ErrorReportDto errorReportDto) {
        CourseDto courseDto = courseService.getByExternalId(errorReportDto.getCourseExternalId());
        String type = splitService
                .getTypeOfCourseByDuration(courseDto.calculateCurseDurationInDays());
        String team = splitService.getTeamThatResponsibleForCourse(courseDto.getIsbn());
        ErrorReport errorReport = ErrorReport.builder()
                                        .courseId(errorReportDto.getCourseExternalId())
                                        .courseDuration(type)
                                        .description(errorReportDto.getDescription())
                                        .team(team).build();

        return errorReportRepository.save(errorReport);
    }

}
