package com.java.nix.supportmicroservice.service;

import com.java.nix.supportmicroservice.entity.dto.CourseDto;

public interface CourseService {

    CourseDto getByExternalId(String externalId);

}
