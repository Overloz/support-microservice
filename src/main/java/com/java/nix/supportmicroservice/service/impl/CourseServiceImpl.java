package com.java.nix.supportmicroservice.service.impl;

import com.java.nix.supportmicroservice.entity.dto.CourseDto;
import com.java.nix.supportmicroservice.exception.CourseNotFoundException;
import com.java.nix.supportmicroservice.service.CourseService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService {

    @Value("${rest.request.path}")
    private String PATH;

    private final RestTemplate restTemplate;

    public CourseServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public CourseDto getByExternalId(String externalId) {
        return Optional
                .ofNullable(restTemplate.getForObject( PATH + externalId, CourseDto.class))
                .orElseThrow(() -> new CourseNotFoundException(externalId));
    }
}
