package com.java.nix.supportmicroservice.service;

import com.java.nix.supportmicroservice.entity.ErrorReport;
import com.java.nix.supportmicroservice.entity.dto.ErrorReportDto;

import java.util.List;

public interface ErrorReportService {

    ErrorReport getById(Long id);

    List<ErrorReport> getAll();

    List<ErrorReport> getAllByCourseId(String courseId);

    ErrorReport saveReport(ErrorReportDto errorReportDto);


}
