package com.java.nix.supportmicroservice.service;

public interface SplitService {

    String getTypeOfCourseByDuration(Long courseDuration);

    String getTeamThatResponsibleForCourse(Long isbn);

}
