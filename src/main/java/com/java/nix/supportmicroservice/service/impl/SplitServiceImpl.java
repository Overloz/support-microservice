package com.java.nix.supportmicroservice.service.impl;

import com.java.nix.supportmicroservice.service.SplitService;
import io.split.client.SplitClient;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

@Log4j2
@Service
public class SplitServiceImpl implements SplitService {

    @Value("${split.team.type}")
    private String TYPE_TEAM;
    @Value("${split.team.name}")
    private String TEAM_SPLIT_NAME;
    @Value("${split.team.attribute}")
    private String TEAM_ATTRIBUTE;
    @Value("${split.course.name}")
    private String COURSE_SPLIT_NAME;
    @Value("${split.course.attribute}")
    private String COURSE_ATTRIBUTE;
    @Value("${split.course.type}")
    private String TYPE_COURSE;

    private final SplitClient splitClient;

    public SplitServiceImpl(SplitClient splitClient) {
        this.splitClient = splitClient;
    }

    private void waitForSplitClient(){
        try {
            splitClient.blockUntilReady();
        } catch (TimeoutException | InterruptedException e) {
            log.info(e.getMessage());
        }
    }

    @Override
    public String getTypeOfCourseByDuration(Long courseDuration) {
        waitForSplitClient();

        Map<String, Object> attributes = new HashMap<>();

        attributes.put(COURSE_ATTRIBUTE, courseDuration);

        return splitClient.getTreatment(TYPE_COURSE, COURSE_SPLIT_NAME, attributes);
    }

    @Override
    public String getTeamThatResponsibleForCourse(Long isbn) {
        waitForSplitClient();

        Map<String, Object> attributes = new HashMap<>();

        attributes.put(TEAM_ATTRIBUTE, isbn);

        return splitClient.getTreatment(TYPE_TEAM, TEAM_SPLIT_NAME, attributes);
    }

}

