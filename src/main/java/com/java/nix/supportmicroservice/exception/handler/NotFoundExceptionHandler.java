package com.java.nix.supportmicroservice.exception.handler;

import com.java.nix.supportmicroservice.entity.dto.ErrorDto;
import com.java.nix.supportmicroservice.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class NotFoundExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ErrorDto> exceptionHandlerUserExists(NotFoundException exception) {
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        ErrorDto errorReport = new ErrorDto(202, System.currentTimeMillis(), exception.getMessage());

        return new ResponseEntity<>(errorReport, httpStatus);
    }

}
