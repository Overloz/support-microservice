package com.java.nix.supportmicroservice.exception;

public class CourseNotFoundException extends NotFoundException {
    public CourseNotFoundException(String externalId) {
        super(String.format("Course with externalId: %s not found", externalId));
    }
}
