package com.java.nix.supportmicroservice.entity.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
public class CourseDto {

    private Long isbn;

    private String externalId;

    private LocalDateTime endingDate;

    private LocalDateTime startingDate;

    public Long calculateCurseDurationInDays() {
        return Duration.between(startingDate, endingDate).toDays();
    }

}