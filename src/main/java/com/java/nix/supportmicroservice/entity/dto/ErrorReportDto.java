package com.java.nix.supportmicroservice.entity.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
public class ErrorReportDto {

    private Long errorCode;

    private String courseExternalId;

    private String description;
}
