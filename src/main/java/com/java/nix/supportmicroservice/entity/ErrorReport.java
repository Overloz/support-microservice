package com.java.nix.supportmicroservice.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Data
@Entity
@Table(name = "error_report")
@SuperBuilder
@NoArgsConstructor
public class ErrorReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Exclude
    private Long id;

    @Column
    private String team;

    @Column
    private String courseId;

    @Column
    private String courseDuration;

    @Column
    private String description;
}
