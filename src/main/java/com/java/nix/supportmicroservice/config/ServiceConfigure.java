package com.java.nix.supportmicroservice.config;

import io.split.client.SplitClient;
import io.split.client.SplitClientConfig;
import io.split.client.SplitFactory;
import io.split.client.SplitFactoryBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URISyntaxException;

@Configuration
public class ServiceConfigure {

    @Value("${api.key}")
    private String apiKey;

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public SplitClient getSplitClient() throws IOException, URISyntaxException {
        SplitClientConfig config = SplitClientConfig.builder()
                .setBlockUntilReadyTimeout(20000)
                .build();
        SplitFactory splitFactory = SplitFactoryBuilder
                .build(apiKey, config);
        return splitFactory.client();
    }


}
