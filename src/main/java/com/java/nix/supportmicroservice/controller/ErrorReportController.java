package com.java.nix.supportmicroservice.controller;

import com.java.nix.supportmicroservice.entity.ErrorReport;
import com.java.nix.supportmicroservice.entity.dto.ErrorReportDto;
import com.java.nix.supportmicroservice.service.ErrorReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/errorReport")
public class ErrorReportController {

    private final ErrorReportService errorReportService;

    @Autowired
    public ErrorReportController(ErrorReportService errorReportService) {
        this.errorReportService = errorReportService;
    }

    @GetMapping("/{id}")
    public ErrorReport getErrorReport(@PathVariable Long id){
        return errorReportService.getById(id);
    }

    @GetMapping
    public List<ErrorReport> getAll(){
        return errorReportService.getAll();
    }

    @GetMapping("/course/{courseId}")
    public List<ErrorReport> getAllByCourseId(@PathVariable String courseId){
        return errorReportService.getAllByCourseId(courseId);
    }

    @PostMapping
    public ErrorReport createErrorReport(@RequestBody ErrorReportDto errorReportDto){
        return errorReportService.saveReport(errorReportDto);
    }
}
