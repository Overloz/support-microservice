package com.java.nix.supportmicroservice.service.impl;

import io.split.client.SplitClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class SplitServiceImplTest {

    @InjectMocks
    private SplitServiceImpl splitService;

    @Mock
    private SplitClient splitClient;

    @Test
    public void getTypeOfCourseByDurationSuccessfullyTest() {
        String type = "course";
        String attribute = "time";
        String schema = "support";
        String typeCourse = "TYPE_TEAM";
        String courseAttribute = "TEAM_ATTRIBUTE";
        String courseSplitName = "COURSE_SPLIT_NAME";
        ReflectionTestUtils.setField(splitService, typeCourse, type);
        ReflectionTestUtils.setField(splitService, courseAttribute, attribute);
        ReflectionTestUtils.setField(splitService, courseSplitName, schema);

        Map<String, Object> attributes = new HashMap<>();

        attributes.put(attribute, 1L);

        lenient().when(splitClient.getTreatment(type, schema, attributes)).thenReturn(schema);

        String result = splitService.getTypeOfCourseByDuration(1L);

        assertEquals(result, schema);
    }

    @Test
    public void getTeamThatResponsibleForCourseSuccessfullyTest() {
        String type = "team";
        String attribute = "isbn";
        String schema = "team";
        String typeTeam = "TYPE_TEAM";
        String teamAttribute = "TEAM_ATTRIBUTE";
        String teamSplitName = "TEAM_SPLIT_NAME";

        ReflectionTestUtils.setField(splitService, typeTeam, type);
        ReflectionTestUtils.setField(splitService, teamAttribute, attribute);
        ReflectionTestUtils.setField(splitService, teamSplitName, schema);

        Map<String, Object> attributes = new HashMap<>();

        attributes.put(attribute, 1L);

        lenient().when(splitClient.getTreatment(type, schema, attributes)).thenReturn(type);

        String result = splitService.getTeamThatResponsibleForCourse(1L);

        assertEquals(result, type);
    }

}