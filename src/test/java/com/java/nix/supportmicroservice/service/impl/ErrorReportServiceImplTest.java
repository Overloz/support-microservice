package com.java.nix.supportmicroservice.service.impl;

import com.java.nix.supportmicroservice.entity.ErrorReport;
import com.java.nix.supportmicroservice.entity.dto.CourseDto;
import com.java.nix.supportmicroservice.entity.dto.ErrorReportDto;
import com.java.nix.supportmicroservice.repository.ErrorReportRepository;
import com.java.nix.supportmicroservice.service.CourseService;
import com.java.nix.supportmicroservice.service.SplitService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ErrorReportServiceImplTest {

    private final static String TEST_STRING = "test";

    @InjectMocks
    private ErrorReportServiceImpl errorReportService;
    @Mock
    private CourseService courseService;
    @Mock
    private SplitService splitService;
    @Mock
    private ErrorReportRepository errorReportRepository;

    private ErrorReport errorReport;

    @BeforeEach
    public void init() {
        this.errorReport = ErrorReport.builder().id(1L).team(TEST_STRING).courseId(TEST_STRING)
                .courseDuration(TEST_STRING).build();
    }

    @Test
    public void getByIdTest() {
        lenient().when(errorReportRepository.findById(1L)).thenReturn(Optional.of(errorReport));

        ErrorReport returned = errorReportService.getById(1L);

        verify(errorReportRepository, times(1)).findById(1L);
        assertEquals(returned, errorReport);
    }

    @Test
    public void getAllTest() {
        List<ErrorReport> toReturn = List.of(errorReport);

        lenient().when(errorReportRepository.findAll()).thenReturn(toReturn);

        List<ErrorReport> returned = errorReportService.getAll();

        verify(errorReportRepository, times(1)).findAll();
        assertEquals(toReturn, returned);
    }

    @Test
    public void createErrorReport() {
        Long isbn = 1111111L;
        ErrorReportDto errorReportDto = ErrorReportDto.builder()
                                                .errorCode(1L)
                                                .courseExternalId(TEST_STRING)
                                                .description(TEST_STRING).build();
        ErrorReport errorReportToReturn = ErrorReport.builder()
                                                .courseId(TEST_STRING)
                                                .courseDuration(TEST_STRING)
                                                .description(TEST_STRING)
                                                .team(TEST_STRING)
                                                .id(1L).build();
        CourseDto courseToReturn = CourseDto.builder()
                                                .isbn(isbn)
                                                .endingDate(LocalDateTime.now())
                                                .startingDate(LocalDateTime.now())
                                                .externalId(TEST_STRING).build();

        lenient().when(courseService.getByExternalId(TEST_STRING)).thenReturn(courseToReturn);
        lenient().when(splitService.getTypeOfCourseByDuration(0L)).thenReturn(TEST_STRING);
        lenient().when(splitService.getTeamThatResponsibleForCourse(isbn)).thenReturn(TEST_STRING);
        lenient().when(errorReportRepository.save(errorReportToReturn)).thenReturn(errorReportToReturn);

        ErrorReport saved = errorReportService.saveReport(errorReportDto);

        assertEquals(errorReportToReturn,saved);
    }
}