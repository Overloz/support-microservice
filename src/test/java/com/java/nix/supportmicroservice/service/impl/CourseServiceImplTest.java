package com.java.nix.supportmicroservice.service.impl;

import com.java.nix.supportmicroservice.entity.dto.CourseDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@TestPropertySource
@ExtendWith(MockitoExtension.class)
class CourseServiceImplTest {

    @InjectMocks
    private CourseServiceImpl courseService;

    @Mock
    private RestTemplate restTemplate;


    @Test
    void getByExternalId() {
        Long isbn = 1111111L;
        String test = "PATH";
        final String path = "http://java-app:8080/java-training/course/external/";
        CourseDto courseToReturn = CourseDto.builder()
                                                .isbn(isbn)
                                                .endingDate(LocalDateTime.now())
                                                .startingDate(LocalDateTime.now())
                                                .externalId(test).build();

        ReflectionTestUtils.setField(courseService, test, path);

        lenient().when(restTemplate.getForObject(path + test, CourseDto.class))
                .thenReturn(courseToReturn);

        CourseDto returned = courseService.getByExternalId(test);

        assertEquals(courseToReturn, returned);
    }
}