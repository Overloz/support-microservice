package com.java.nix.supportmicroservice.controller;

import com.java.nix.supportmicroservice.entity.ErrorReport;
import com.java.nix.supportmicroservice.entity.dto.ErrorReportDto;
import com.java.nix.supportmicroservice.service.ErrorReportService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class ErrorReportControllerTest {

    private final static String TEST_STRING = "test";

    @Mock
    private ErrorReportService errorReportService;

    private MockMvc mockMvc;

    private ErrorReport errorReport;

    @BeforeEach
    void setup() {
        errorReport = ErrorReport.builder().id(1L).courseDuration(TEST_STRING).team(TEST_STRING)
                .courseId(TEST_STRING).build();
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(new ErrorReportController(errorReportService)).build();
    }

    @Test
    public void getErrorReport() throws Exception {
        lenient().when(errorReportService.getById(1L)).thenReturn(errorReport);

        mockMvc.perform(get("/errorReport/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        verify(errorReportService, times(1)).getById(1L);
    }

    @Test
    public void getAllSuccesfullyTest() throws Exception{
        lenient().when(errorReportService.getAllByCourseId(TEST_STRING)).thenReturn(List.of(errorReport));

        mockMvc.perform(get("/errorReport/course/test").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        verify(errorReportService, times(1)).getAllByCourseId(TEST_STRING);
    }

    @Test
    public void createErrorReport() throws Exception {
        ErrorReportDto errorReportDto = ErrorReportDto.builder()
                                                .errorCode(1L)
                                                .description(TEST_STRING)
                                                .courseExternalId(TEST_STRING).build();
        lenient().when(errorReportService.saveReport(errorReportDto)).thenReturn(errorReport);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/errorReport")
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(getJsonErrorReport());

        mockMvc.perform(builder).andExpect(status().isOk());
    }

    private String getJsonErrorReport(){
        return "{\n" + "  \"courseExternalId\": \"test\",\n" + "  \"description\": \"description\",\n"
                + "  \"errorCode\": 1\n" + "}";
    }

}